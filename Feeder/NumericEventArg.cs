﻿namespace Feeder
{
    public class NumericEventArg
    {
        public int CurrentValue
        {
            get;
        }

        public int MinValue
        {
            get;
        }

        public int MaxValue
        {
            get;
        }

        public NumericEventArg (int current, int min, int max)
        {
            CurrentValue = current;
            MinValue = min;
            MaxValue = max;
        }
    }
}