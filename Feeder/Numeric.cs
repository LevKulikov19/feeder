﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Feeder
{
    public partial class Numeric : UserControl
    {
        public delegate void NumericEvent(object sender, NumericEventArg e);
        public event NumericEvent ChengeValue;

        private bool buttonEnabled = true;
        public bool ButtonEnabled
        {
            get => buttonEnabled;
            set
            {
                buttonEnabled = value;
                UpdateStateLable();
                Refresh();
            }
        }

        public Color ColorButton
        {
            get;
            set;
        }

        bool disebleButtonUp;
        bool disebleButtonDown;
        Color colorButtonUp;
        Color colorButtonDown;

        public int MinValue
        {
            get;
            set;
        }

        public int MaxValue
        {
            get;
            set;
        }

        int currentValue;
        public int CurrentValue
        {
            get => currentValue;
            set
            {
                currentValue = value;
                labelNum.Text = currentValue.ToString();
            }
        }

        public string LabelText
        {
            get => labelText.Text;
            set
            {
                labelText.Text = value;
            }
        }

        public Numeric()
        {
            InitializeComponent();
            ButtonDown.Width = ButtonDown.Height;
            ButtonUp.Width = ButtonUp.Height;
            ColorButton = ColorTranslator.FromHtml("#C1F6FC");
            ForeColor = Color.White;
            ButtonEnabled = true;
            DrawButtonUp();
            DrawButtonDown();
        }

        private void ButtonUp_Paint(object sender, PaintEventArgs e)
        {
            DrawButtonUp(e.Graphics);
        }

        private void ButtonDown_Paint(object sender, PaintEventArgs e)
        {
            DrawButtonDown(e.Graphics);
        }

        private void DrawButtonDown(Graphics graphics = null)
        {
            BufferedGraphics bufferedGraphics = null;
            if (graphics == null)
            {
                Graphics graphicsPaintSpace = this.CreateGraphics();
                BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
                bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
                graphics = bufferedGraphics.Graphics;
            }

            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            graphics.Clear(BackColor);

            UpdateStateButton();
            DrawTriange(graphics, ButtonDown, colorButtonDown, false);

            if (graphics == null)
            {
                bufferedGraphics.Render(graphics);
                bufferedGraphics.Dispose();
                graphics.Dispose();
            }
        }

        private void DrawButtonUp(Graphics graphics = null)
        {
            BufferedGraphics bufferedGraphics = null;
            if (graphics == null)
            {
                Graphics graphicsPaintSpace = this.CreateGraphics();
                BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
                bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
                graphics = bufferedGraphics.Graphics;
            }

            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            graphics.Clear(BackColor);

            UpdateStateButton();
            DrawTriange(graphics, ButtonUp, colorButtonUp, true);

            if (graphics == null)
            {
                bufferedGraphics.Render(graphics);
                bufferedGraphics.Dispose();
                graphics.Dispose();
            }
        }

        private void DrawTriange(Graphics graphics, Control control, Color color, bool up)
        {
            Point[] points = new Point[3];
            if (up)
            {
                points[0] = new Point(0, control.ClientRectangle.Height);
                points[1] = new Point(control.ClientRectangle.Width / 2, 0);
                points[2] = new Point(control.ClientRectangle.Width, control.ClientRectangle.Height);
            }
            else
            {
                points[0] = new Point(0, 0);
                points[1] = new Point(control.ClientRectangle.Width / 2, control.ClientRectangle.Height);
                points[2] = new Point(control.ClientRectangle.Width, 0);
            }
            Brush brush = new SolidBrush(color);
            graphics.FillPolygon(brush, points);
        }

        private void ButtonUp_Click(object sender, EventArgs e)
        {
            if (!disebleButtonUp && ButtonEnabled)
            {
                CurrentValue++;
                EmmitEventValueChenge();
            }
            UpdateStateButton();
        }

        private void ButtonDown_Click(object sender, EventArgs e)
        {
            if (!disebleButtonDown && ButtonEnabled)
            {
                CurrentValue--;
                EmmitEventValueChenge();
            }
            UpdateStateButton();
        }

        private void UpdateStateButton ()
        {
            disebleButtonUp = false;
            disebleButtonDown = false;
            if (CurrentValue <= MinValue)
            {
                disebleButtonDown = true;
                colorButtonDown = ColorButton;
                DarkenColor(colorButtonDown, 10);
            }
            if (CurrentValue >= MaxValue)
            {
                disebleButtonUp = true;
                colorButtonUp = ColorButton;
                DarkenColor(colorButtonUp, 10);
            }
        }

        private Color DarkenColor(Color colorIn, uint percent)
        {
            if (percent > 100) percent = 100;

            int a, r, g, b;
            a = colorIn.A;
            r = colorIn.R - (int)((colorIn.R / 100f) * percent);
            g = colorIn.G - (int)((colorIn.G / 100f) * percent);
            b = colorIn.B - (int)((colorIn.B / 100f) * percent);

            return Color.FromArgb(a, r, g, b);
        }

        private Color LightenColor(Color colorIn, uint percent)
        {
            if (percent > 100) percent = 100;

            int a, r, g, b;
            a = colorIn.A;
            r = colorIn.R + (int)(((255f - colorIn.R) / 100f) * percent);
            g = colorIn.G + (int)(((255f - colorIn.G) / 100f) * percent);
            b = colorIn.B + (int)(((255f - colorIn.B) / 100f) * percent);

            return Color.FromArgb(a, r, g, b);
        }

        private void ButtonUp_MouseHover(object sender, EventArgs e)
        {
            colorButtonUp = LightenColor(colorButtonUp, 50);
            UpdateStateButton();
            ButtonUp.Refresh();
        }

        private void ButtonDown_MouseHover(object sender, EventArgs e)
        {
            colorButtonDown = LightenColor(colorButtonDown, 50);
            UpdateStateButton();
            ButtonDown.Refresh();
        }

        private void ButtonUp_MouseLeave(object sender, EventArgs e)
        {
            colorButtonUp = ColorButton;
            UpdateStateButton();
            ButtonUp.Refresh();
        }

        private void ButtonDown_MouseLeave(object sender, EventArgs e)
        {
            colorButtonDown = ColorButton;
            UpdateStateButton();
            ButtonDown.Refresh();
        }

        private void ButtonUp_MouseDown(object sender, MouseEventArgs e)
        {
            colorButtonUp = DarkenColor(colorButtonDown, 10);
            UpdateStateButton();
            ButtonUp.Refresh();
        }

        private void ButtonDown_MouseDown(object sender, MouseEventArgs e)
        {
            colorButtonDown = DarkenColor(colorButtonDown, 10);
            UpdateStateButton();
            ButtonDown.Refresh();
        }

        private void ButtonUp_MouseUp(object sender, MouseEventArgs e)
        {
            colorButtonUp = ColorButton;
            UpdateStateButton();
            ButtonUp.Refresh();
        }

        private void ButtonDown_MouseUp(object sender, MouseEventArgs e)
        {
            colorButtonDown = ColorButton;
            UpdateStateButton();
            ButtonDown.Refresh();
        }

        private void Numeric_Paint(object sender, PaintEventArgs e)
        {
            ButtonUp.Refresh();
        }

        private void EmmitEventValueChenge()
        {
            NumericEventArg arg = new NumericEventArg(CurrentValue, MinValue, MaxValue);
            if (ChengeValue != null) ChengeValue(this, arg);
        }

        private void UpdateStateLable()
        {
            if (ButtonEnabled)
            {
                labelNum.ForeColor = ForeColor;
                labelText.ForeColor = ForeColor;
            }
            else
            {

                labelNum.ForeColor = DarkenColor(ForeColor, 20);
                labelText.ForeColor = DarkenColor(ForeColor, 20);
            }
        }
    }
}
