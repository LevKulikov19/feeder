﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Feeder
{
    class Bird
    {
        Random randomPeac = new Random();
        public static Semaphore semaphore;
        bool isWait = false;
        public static int id;
        Thread myThread;

        public delegate void BirdEvent(object sender);
        public event BirdEvent FlyToFeeder;
        public event BirdEvent Sitting;
        public event BirdEvent Sit;
        public event BirdEvent PeckEnd; 

        public Point Location;

        public uint FeedingTimeMilliSeconds
        {
            get;
            set;
        }

        enum AnimationType
        {
            None,
            Fly,
            Peck,
            Sit,
            Sitting
        }

        System.Timers.Timer timerAnimation;
        System.Timers.Timer timerPeck;

        Point endPoint;
        AnimationType animType;
        AnimationType AnimationState
        {
            get => animType;
            set
            {
                    lock (imageSpriteLock)
                    {
                        animType = value;
                        switch (AnimationState)
                        {
                            case AnimationType.None:
                                imageSprite = Properties.Resources.Empty;
                                frameCountBirdSprite = 0;
                                break;
                            case AnimationType.Fly:

                                imageSprite = Properties.Resources.BirdFlySprite;
                                frameCountBirdSprite = 8;
                                break;
                            case AnimationType.Peck:
                                imageSprite = Properties.Resources.BirdPeakSprite;
                                frameCountBirdSprite = 12;
                                break;
                            case AnimationType.Sit:
                                imageSprite = Properties.Resources.BirdSitSprite;
                                frameCountBirdSprite = 1;
                                break;
                            case AnimationType.Sitting:
                                imageSprite = Properties.Resources.BirdSittingSprite;
                                frameCountBirdSprite = 7;
                                break;
                            default:
                                imageSprite = Properties.Resources.Empty;
                                frameCountBirdSprite = 0;
                                break;
                        }
                        frameBirdSprite = 0;
                        if (!ImageBirdLeft) imageSprite.RotateFlip(RotateFlipType.RotateNoneFlipX);
                    }
                

            }
        }
        bool animation;
        uint speed;
        uint durationPeck;
        Point pointFeeder;

        private readonly object imageSpriteLock = new object();
        Image imageSprite = Properties.Resources.Empty;
        uint imageWidthBird = 256;
        uint imageHeightBird = 256;
        bool imageBirdLeft = true;
        bool ImageBirdLeft
        {
            get => imageBirdLeft;
            set
            {
                lock (imageSpriteLock)
                {
                    if (imageSprite != null && imageBirdLeft != value) imageSprite.RotateFlip(RotateFlipType.RotateNoneFlipX);
                    imageBirdLeft = value;
                }
            }
        }
        uint frameCountBirdSprite;
        uint frameBirdSprite;

        public Bird () : base()
        {
            AnimationState = AnimationType.None;

            timerAnimation = new System.Timers.Timer();
            timerAnimation.Interval = 33;
            timerAnimation.Enabled = false;
            timerAnimation.Elapsed += timerAnimation_Tick;


            timerPeck = new System.Timers.Timer();
            timerPeck.Enabled = false;
            timerPeck.Elapsed += TimerPeck_Tick; ;

            animation = false;
            speed = 5;
            Sitting += Bird_Sitting;
            Sit += Bird_Sit;
        }

        public void HideBird ()
        {
            timerAnimation.Enabled = false;
            AnimationState = AnimationType.None;
        }

        private void TimerPeck_Tick(object sender, EventArgs e)
        {
            AnimationState = AnimationType.Sit;
            if (PeckEnd != null) PeckEnd(this);
            timerPeck.Enabled = false;
        }

        private void Bird_Sit(object sender)
        {
            AnimationState = AnimationType.Sit;
        }

        private void Bird_Sitting(object sender)
        {
            AnimationState = AnimationType.Sitting;
        }

        public void FlyTo(Point endPoint, uint speedFly = 5)
        {
            speed = speedFly;
            AnimationState = AnimationType.Fly;
            animation = true;
            endPoint.Offset(-60, -71);
            this.endPoint = endPoint;
            timerAnimation.Enabled = true;
        }

        public void FlyAndPeck(Point endPoint, uint duration)
        {
            pointFeeder = endPoint;
            durationPeck = (uint)(duration + randomPeac.Next(0, 5));
            Sit += Bird_Sit_Peck;
            PeckEnd += Bird_PeckEnd;
            myThread = new Thread(FlyFeeder);
            myThread.Name = "bird " + id.ToString();
            id++;
            myThread.IsBackground = true;
            myThread.Start();
        }

        private void FlyFeeder()
        {
            try
            {
                isWait = true;
                semaphore.WaitOne();
                FlyTo(pointFeeder);
                if (FlyToFeeder != null) FlyToFeeder(this);
            }
            catch(Exception)
            {
                
            }
        }

        private void Bird_PeckEnd(object sender)
        {
            Sit -= Bird_Sit_Peck;
            PeckEnd -= Bird_PeckEnd;
            semaphore.Release();
            isWait = false;
        }

        private void Bird_Sit_Peck(object sender)
        {
            imageBirdLeft = false;
            Peck(durationPeck);
        }

        public void Peck(uint duration)
        {
            AnimationState = AnimationType.Peck;
            timerPeck.Interval = (int)duration;
            timerPeck.Enabled = true;

        }     

        private void timerAnimation_Tick(object sender, EventArgs e)
        {
            if (AnimationState == AnimationType.Fly) AnimationFlyTick();
            else if (AnimationState == AnimationType.Sitting) AnimationSittingTick();
        }

        private void AnimationFlyTick ()
        {
            if (this.endPoint == Location)
            {
                animation = false;
                timerAnimation.Enabled = false;
            }
            else
            {
                bool sitting = false;
                Point newLocation = NewFlyLocation(Location, endPoint, speed, ref sitting);
                if ((((newLocation - (Size)Location).X < 0) && ImageBirdLeft) || (((newLocation - (Size)Location).X > 0) && !ImageBirdLeft))
                {
                    ImageBirdLeft = !ImageBirdLeft;
                }

                Location = newLocation;
                if (sitting && frameBirdSprite == 1 && Sitting != null) Sitting(this);
            }
        }

        private void AnimationSittingTick()
        {
            Location = NewSittingLocation(Location, endPoint, speed);
            if (frameCountBirdSprite == frameBirdSprite)
            {
                imageBirdLeft = false;
                if (Sit != null) Sit(this);
            }
        }

        public void DrawBird(Graphics graphics)
        {
            if (AnimationState == AnimationType.None) return;
            Image imageTemp;
            lock (imageSpriteLock)
            {
                imageTemp = (Image)imageSprite.Clone();
            }
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            if (frameCountBirdSprite <= frameBirdSprite) frameBirdSprite = 0;

            int xBirdSprite = 0;
            if (imageBirdLeft) xBirdSprite = (int)(frameBirdSprite * imageWidthBird);
            else
            {
                xBirdSprite = imageTemp.Width - (int)((frameBirdSprite + 1) * imageWidthBird);
            }
            int yBirdSprite = 0;
            ImageAttributes wrapMode = new ImageAttributes();
            Rectangle rectangleViewBird = new Rectangle(Location.X, Location.Y, 128, 128);

            graphics.DrawImage(imageTemp, rectangleViewBird, xBirdSprite, yBirdSprite, imageWidthBird, imageHeightBird, GraphicsUnit.Pixel, wrapMode);
            

            if (animation) frameBirdSprite++;
            imageTemp.Dispose();
        }

        private Point NewFlyLocation(Point currentLocation, Point endLocation, uint speed, ref bool sitting)
        {
            Point result;
            Point dif = currentLocation - ((Size)endLocation);
            if (Math.Abs(dif.X) < 15 && Math.Abs(dif.Y) < 15) sitting = true;
            else sitting = false;
            result = new Point(
                (int)Math.Round(currentLocation.X - (dif.X * speed / 100.0)),
                (int)Math.Round(currentLocation.Y - (dif.Y * speed / 100.0)));
            return result;
        }

        private Point NewSittingLocation(Point currentLocation, Point endLocation, uint speed)
        {
            if (currentLocation == endLocation) return endLocation;
            Point result;
            Point dif = currentLocation - ((Size)endLocation);
            result = new Point(
                (int)Math.Round(currentLocation.X - (dif.X * speed / 10.0)),
                (int)Math.Round(currentLocation.Y - (dif.Y * speed / 10.0)));
            return result;
        }

        public void Stop()
        {
            if (isWait) myThread.Interrupt();
            timerAnimation.Stop();
            timerAnimation.Dispose();
            timerPeck.Stop();
            timerPeck.Dispose();
        }
    }
}
