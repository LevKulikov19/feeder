﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;

namespace Feeder
{
    class BirdsQueue
    {
        public delegate void BirdsQueueEvent(object sender, Bird bird);
        public event BirdsQueueEvent BirdToTree;

        public Point Location;
        Random randomBird;
        Dictionary<Point, Bird> plases;
        Queue<Bird> birdQueue;
        float sizeFactor = 0.66F;

        Image imageBack;
        public BirdsQueue () : base ()
        {
            imageBack = Properties.Resources.RightTree;

            randomBird = new Random();

            birdQueue = new Queue<Bird>();
            plases = new Dictionary<Point, Bird>();
            FillPlaces(plases);
        }

        public void DrawBirdsQueue (Graphics graphics)
        {
            ImageAttributes wrapMode = new ImageAttributes();
            Size sizeImage = new Size((int)(imageBack.Width * sizeFactor), (int)(imageBack.Height * sizeFactor));
            Rectangle rectangleView = new Rectangle(Location, sizeImage);
            graphics.DrawImage(imageBack, rectangleView, 0, 0, imageBack.Width, imageBack.Height, GraphicsUnit.Pixel, wrapMode);
        }

        public bool existFreePlace()
        {
            foreach (var item in plases.Values)
            {
                if (item == null)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Возвращает абсолютные координаты свободного места
        /// </summary>
        public Point GetFreePlace(Bird bird)
        {
            Point result = Point.Empty;
            foreach (var item in plases)
            {
                if (item.Value == null)
                {
                    result = item.Key;
                    plases[item.Key] = bird;
                    break;
                }
            }
            result = new Point(Location.X + result.X, Location.Y + result.Y);
            return result;
        }

        public void AddToQueue (Bird bird)
        {
            birdQueue.Enqueue(bird);
            if (existFreePlace())
            {
                BirdToTreeFromQueue();
            }
        }

        public bool ReturnFreePlace(Bird bird)
        {
            if (!plases.ContainsValue(bird))
                return false;
            foreach (var item in plases)
            {
                if (item.Value == bird)
                {
                    plases[item.Key] = null;
                    break;
                }
            }
            BirdToTreeFromQueue();
            return true;
        }

        private bool BirdToTreeFromQueue()
        {
            if (birdQueue.Count == 0) return false;
            Bird bird = birdQueue.Dequeue();
            BirdToTree(this, bird);

            return true;
        }

        public Bird GetBird ()
        {
            List<Bird> birds = new List<Bird>();
            foreach (Bird item in plases.Values)
            {
                if (item != null) birds.Add(item);
            }
            if (birds.Count == 0) return null;
            Bird bird = birds[randomBird.Next(0, birds.Count)];
            ReturnFreePlace(bird);
            return bird;
        }

        public void Clear()
        {
            randomBird = new Random();
            plases.Clear();
            birdQueue.Clear();
            FillPlaces(plases);
        }

        public void FillPlaces(Dictionary<Point, Bird>  p)
        {
            p.Add(new Point((int)Math.Round(50 * sizeFactor), (int)Math.Round(600 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(100 * sizeFactor), (int)Math.Round(580 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(130 * sizeFactor), (int)Math.Round(655 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(140 * sizeFactor), (int)Math.Round(560 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(190 * sizeFactor), (int)Math.Round(655 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(250 * sizeFactor), (int)Math.Round(660 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(300 * sizeFactor), (int)Math.Round(645 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(325 * sizeFactor), (int)Math.Round(610 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(280 * sizeFactor), (int)Math.Round(420 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(240 * sizeFactor), (int)Math.Round(340 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(200 * sizeFactor), (int)Math.Round(320 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(260 * sizeFactor), (int)Math.Round(225 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(200 * sizeFactor), (int)Math.Round(180 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(245 * sizeFactor), (int)Math.Round(135 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(340 * sizeFactor), (int)Math.Round(240 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(450 * sizeFactor), (int)Math.Round(220 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(430 * sizeFactor), (int)Math.Round(490 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(455 * sizeFactor), (int)Math.Round(415 * sizeFactor)), null);
            p.Add(new Point((int)Math.Round(510 * sizeFactor), (int)Math.Round(365 * sizeFactor)), null);
        }
    }
}
