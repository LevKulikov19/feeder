﻿
namespace Feeder
{
    partial class FormMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.timerDraw = new System.Windows.Forms.Timer(this.components);
            this.timerBird = new System.Windows.Forms.Timer(this.components);
            this.numericTimePeac = new Feeder.Numeric();
            this.numericAmountFlying = new Feeder.Numeric();
            this.numericСapacity = new Feeder.Numeric();
            this.buttonMain = new Feeder.ButtonMain();
            this.SuspendLayout();
            // 
            // timerDraw
            // 
            this.timerDraw.Enabled = true;
            this.timerDraw.Interval = 33;
            this.timerDraw.Tick += new System.EventHandler(this.timerDraw_Tick);
            // 
            // timerBird
            // 
            this.timerBird.Enabled = true;
            this.timerBird.Tick += new System.EventHandler(this.timerBird_Tick);
            // 
            // numericTimePeac
            // 
            this.numericTimePeac.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(197)))), ((int)(((byte)(62)))));
            this.numericTimePeac.ButtonEnabled = true;
            this.numericTimePeac.ColorButton = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(246)))), ((int)(((byte)(252)))));
            this.numericTimePeac.CurrentValue = 5;
            this.numericTimePeac.ForeColor = System.Drawing.Color.White;
            this.numericTimePeac.LabelText = "Время кормления";
            this.numericTimePeac.Location = new System.Drawing.Point(12, 602);
            this.numericTimePeac.MaxValue = 5;
            this.numericTimePeac.MinValue = 1;
            this.numericTimePeac.Name = "numericTimePeac";
            this.numericTimePeac.Size = new System.Drawing.Size(304, 50);
            this.numericTimePeac.TabIndex = 0;
            this.numericTimePeac.ChengeValue += new Feeder.Numeric.NumericEvent(this.numericTimePeac_ChengeValue);
            // 
            // numericAmountFlying
            // 
            this.numericAmountFlying.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(197)))), ((int)(((byte)(62)))));
            this.numericAmountFlying.ButtonEnabled = true;
            this.numericAmountFlying.ColorButton = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(246)))), ((int)(((byte)(252)))));
            this.numericAmountFlying.CurrentValue = 15;
            this.numericAmountFlying.ForeColor = System.Drawing.Color.White;
            this.numericAmountFlying.LabelText = "Частота подлета";
            this.numericAmountFlying.Location = new System.Drawing.Point(322, 602);
            this.numericAmountFlying.MaxValue = 15;
            this.numericAmountFlying.MinValue = 1;
            this.numericAmountFlying.Name = "numericAmountFlying";
            this.numericAmountFlying.Size = new System.Drawing.Size(285, 50);
            this.numericAmountFlying.TabIndex = 1;
            this.numericAmountFlying.ChengeValue += new Feeder.Numeric.NumericEvent(this.numericAmountFlying_ChengeValue);
            // 
            // numericСapacity
            // 
            this.numericСapacity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(197)))), ((int)(((byte)(62)))));
            this.numericСapacity.ButtonEnabled = true;
            this.numericСapacity.ColorButton = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(246)))), ((int)(((byte)(252)))));
            this.numericСapacity.CurrentValue = 6;
            this.numericСapacity.ForeColor = System.Drawing.Color.White;
            this.numericСapacity.LabelText = "Вместимость кормушки";
            this.numericСapacity.Location = new System.Drawing.Point(713, 602);
            this.numericСapacity.MaxValue = 6;
            this.numericСapacity.MinValue = 1;
            this.numericСapacity.Name = "numericСapacity";
            this.numericСapacity.Size = new System.Drawing.Size(364, 50);
            this.numericСapacity.TabIndex = 2;
            this.numericСapacity.ChengeValue += new Feeder.Numeric.NumericEvent(this.numericСapacity_ChengeValue);
            // 
            // buttonMain
            // 
            this.buttonMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(197)))), ((int)(((byte)(62)))));
            this.buttonMain.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(197)))), ((int)(((byte)(62)))));
            this.buttonMain.ButtonText = "Начать";
            this.buttonMain.Location = new System.Drawing.Point(1083, 602);
            this.buttonMain.Name = "buttonMain";
            this.buttonMain.Size = new System.Drawing.Size(163, 50);
            this.buttonMain.TabIndex = 3;
            this.buttonMain.ClickButton += new Feeder.ButtonMain.ButtonMainEvent(this.buttonMain_ClickButton);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1258, 664);
            this.Controls.Add(this.buttonMain);
            this.Controls.Add(this.numericСapacity);
            this.Controls.Add(this.numericAmountFlying);
            this.Controls.Add(this.numericTimePeac);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "Кормушка";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FormMain_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerDraw;
        private System.Windows.Forms.Timer timerBird;
        private Numeric numericTimePeac;
        private Numeric numericAmountFlying;
        private Numeric numericСapacity;
        private ButtonMain buttonMain;
    }
}

