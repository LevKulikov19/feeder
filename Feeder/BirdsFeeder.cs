﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;

namespace Feeder
{
    class BirdsFeeder
    {
        public Point Location;

        List<Point> plases;
        float sizeFactor = 0.5F;
        int maxBird = 6;
        int currentBird = 0;
        Random randomBird = new Random();

        Image imageBack = null;
        public BirdsFeeder () : base ()
        {
            imageBack = Properties.Resources.Feeder;
            plases = new List<Point>();
            plases.Add(new Point((int)Math.Round(110 * sizeFactor), (int)Math.Round((125 * sizeFactor))));
            plases.Add(new Point((int)Math.Round(130 * sizeFactor), (int)Math.Round((125 * sizeFactor))));
            plases.Add(new Point((int)Math.Round(160 * sizeFactor), (int)Math.Round((125 * sizeFactor))));
            plases.Add(new Point((int)Math.Round(190 * sizeFactor), (int)Math.Round((125 * sizeFactor))));
            plases.Add(new Point((int)Math.Round(220 * sizeFactor), (int)Math.Round((125 * sizeFactor))));
            plases.Add(new Point((int)Math.Round(250 * sizeFactor), (int)Math.Round((125 * sizeFactor))));
        }

        public void DrawFeeder(Graphics graphics)
        {
            ImageAttributes wrapMode = new ImageAttributes();
            Size sizeImage = new Size((int)(imageBack.Width * sizeFactor), (int)(imageBack.Height * sizeFactor));
            Rectangle rectangleView = new Rectangle(Location, sizeImage);
            graphics.DrawImage(imageBack, rectangleView, 0, 0, imageBack.Width, imageBack.Height, GraphicsUnit.Pixel, wrapMode);
        }

        public bool existFreePlace()
        {
            return currentBird < maxBird;
        }

        public Point GetPlace()
        {
            currentBird++;
            Point result = plases[randomBird.Next(0, plases.Count)];
            result = new Point(Location.X + result.X, Location.Y + result.Y);
            return result;
        }

        public bool returnPlace()
        {
            currentBird--;
            return true;
        }

        public void Clear()
        {
            currentBird = 0;
        }
    }
}
