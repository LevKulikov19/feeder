﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Feeder
{
    public partial class FormMain : Form
    {
        Random random = new Random();

        bool work = false;
        Color buttonMainStart;
        Color buttonMainStop;

        private BirdsFeeder birdsFeeder;
        private BirdsQueue birdsQueue;
        private List<Bird> birds;

        private Point birdStartPoint;
        private Point birdEndPoint;

        private uint durationPeak;

        public FormMain()
        {

            birdsFeeder = new BirdsFeeder();
            birdsFeeder.Location = new Point(60, 234);

            birdsQueue = new BirdsQueue();
            birdsQueue.BirdToTree += BirdsQueue_BirdToTree;
            birdsQueue.Location = new Point(908, -68);

            birds = new List<Bird>();

            InitializeComponent();

            birdStartPoint = new Point(ClientRectangle.Width, 50);
            birdEndPoint = new Point(-ClientRectangle.Width/4, -50);

            buttonMainStart = ColorTranslator.FromHtml("#7DC53E");
            buttonMainStop = ColorTranslator.FromHtml("#C53E3E");

            if (work) ButtonMainStopState();
            else ButtonMainStartState();

            durationPeak = (uint)numericTimePeac.CurrentValue;
            if (numericAmountFlying.CurrentValue < 1)
            {

                timerBird.Interval = numericAmountFlying.MaxValue * 1000;
            }
            else
            {
                timerBird.Interval = numericAmountFlying.MaxValue * 1000 - numericAmountFlying.CurrentValue * 1000 + 500;
            }
            timerBird.Enabled = work;
        }

        private void BirdsQueue_BirdToTree(object sender, Bird bird)
        {
            Point position = (sender as BirdsQueue).GetFreePlace(bird);
            bird.Sit -= Bird_Sit_Queue_Hide;
            bird.FlyTo(position);
            bird.Sit += Bird_Sit_Queue;
        }

        private void DrawBackground(Graphics graphics)
        {
            graphics.Clear(BackColor);
            ImageAttributes wrapMode = new ImageAttributes();
            Image image = Properties.Resources.Background;
            graphics.DrawImage(image, ClientRectangle, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
            image.Dispose();
        }

        private void FormMain_Paint(object sender, PaintEventArgs e)
        {
            Draw();

        }

        private void timerDraw_Tick(object sender, EventArgs e)
        {
            Draw();
        }

        private void Draw ()
        {
            Graphics graphicsPaintSpace = this.CreateGraphics();
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
            Graphics gPaint = bufferedGraphics.Graphics;

            DrawBackground(gPaint);
            birdsFeeder.DrawFeeder(gPaint);
            birdsQueue.DrawBirdsQueue(gPaint);
            Bird[] temp = birds.ToArray();
            foreach (Bird item in temp)
            {
                item.DrawBird(gPaint);
            }
            bufferedGraphics.Render(graphicsPaintSpace);
            bufferedGraphics.Dispose();
            gPaint.Dispose();
        }

        private void timerBird_Tick(object sender, EventArgs e)
        {
            NewBird();
        }

        private void NewBird()
        {
            Bird bird = new Bird();
            Point startPos = new Point(birdStartPoint.X, birdStartPoint.Y + random.Next(-100, 100));
            if (birdsFeeder.existFreePlace())
            {
                Point point = birdsFeeder.GetPlace();
                bird.Location = startPos;
                bird.FlyAndPeck(point, durationPeak * 1000);

                bird.PeckEnd += Bird_PeacEnd;
                birds.Add(bird);
            }
            else
            {
                bird.Location = birdStartPoint;
                Point endPos = new Point(50 + random.Next(-1000, 1000), -100);
                bird.FlyTo(endPos, 3);
                bird.Sit += Bird_Sit_Queue_Hide;
                birdsQueue.AddToQueue(bird);
                birds.Add(bird);
            }
        }

        private void Bird_Sit_Queue_Hide(object sender)
        {
            Bird bird = (sender as Bird);
            bird.HideBird();
        }

        private void Bird_Sit_Queue(object sender)
        {
            Bird bird = (sender as Bird);
            Point point = birdsFeeder.GetPlace();
            bird.FlyAndPeck(point, durationPeak * 1000);
            bird.Sit -= Bird_Sit_Queue;
            bird.PeckEnd += Bird_PeacEnd;
            bird.FlyToFeeder += Bird_FlyToFeeder;
        }

        private void Bird_FlyToFeeder(object sender)
        {
            birdsQueue.ReturnFreePlace(sender as Bird);
        }

        private void Bird_PeacEnd(object sender)
        {
            Bird bird = (sender as Bird);
            bird.Sit += Bird_Sit_End;

            birdsFeeder.returnPlace();
            bird.FlyTo(birdEndPoint);
        }

        private void Bird_Sit_End(object sender)
        {
            Bird bird = (sender as Bird);
            //bird.HideBird();
            bird.Sit -= Bird_Sit_End;
            bird.PeckEnd -= Bird_PeacEnd;
            birds.Remove(bird);
        }

        private void numericTimePeac_ChengeValue(object sender, NumericEventArg e)
        {
            if (e.CurrentValue < 1) durationPeak = 1;
            else durationPeak = (uint)e.CurrentValue;
        }

        private void numericAmountFlying_ChengeValue(object sender, NumericEventArg e)
        {
            if (e.CurrentValue < 1)
            {

                timerBird.Interval = e.MaxValue * 1000;
            }
            else
            {
                timerBird.Interval = e.MaxValue * 1000 - e.CurrentValue * 1000 + 500;
            }
        }
        private void numericСapacity_ChengeValue(object sender, NumericEventArg e)
        {

        }

        private void ButtonMainStopState()
        {
            buttonMain.ButtonColor = buttonMainStop;
            buttonMain.ButtonText = "Остановить";
        }

        private void ButtonMainStartState()
        {
            buttonMain.ButtonColor = buttonMainStart;
            buttonMain.ButtonText = "Начать";
        }

        private void buttonMain_ClickButton(object sender)
        {
            work = !work;
            if (work)
            {
                numericСapacity.ButtonEnabled = false;
                ButtonMainStopState();
                Start();
            }
            else
            {
                numericСapacity.ButtonEnabled = true;
                ButtonMainStartState();
                Stop();
            }
        }

        private void Start()
        {
            Bird.semaphore = new Semaphore(numericСapacity.CurrentValue, numericСapacity.MaxValue, "semaphore " + Bird.id.ToString());
            birds = new List<Bird>();
            NewBird();
            timerBird.Enabled = true;
        }

        private void Stop()
        {
            timerBird.Enabled = false;
            foreach (var item in birds)
            {
                item.Stop();
            }
            Bird.semaphore.Dispose();
            birds.Clear();
            birdsQueue.Clear();
            birdsFeeder.Clear();
        }
    }
}
