﻿
namespace Feeder
{
    partial class ButtonMain
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelText
            // 
            this.labelText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelText.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelText.ForeColor = System.Drawing.Color.White;
            this.labelText.Location = new System.Drawing.Point(0, 0);
            this.labelText.Name = "labelText";
            this.labelText.Size = new System.Drawing.Size(150, 150);
            this.labelText.TabIndex = 0;
            this.labelText.Text = "Начать";
            this.labelText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelText.Click += new System.EventHandler(this.labelText_Click);
            this.labelText.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonMain_MouseDown);
            this.labelText.MouseLeave += new System.EventHandler(this.ButtonMain_MouseLeave);
            this.labelText.MouseHover += new System.EventHandler(this.ButtonMain_MouseHover);
            this.labelText.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ButtonMain_MouseUp);
            // 
            // ButtonMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(197)))), ((int)(((byte)(62)))));
            this.Controls.Add(this.labelText);
            this.Name = "ButtonMain";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonMain_MouseDown);
            this.MouseLeave += new System.EventHandler(this.ButtonMain_MouseLeave);
            this.MouseHover += new System.EventHandler(this.ButtonMain_MouseHover);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ButtonMain_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelText;
    }
}
