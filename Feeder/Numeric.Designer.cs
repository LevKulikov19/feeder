﻿
using System.Drawing;

namespace Feeder
{
    partial class Numeric
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelText = new System.Windows.Forms.Label();
            this.ButtonUp = new System.Windows.Forms.Panel();
            this.ButtonDown = new System.Windows.Forms.Panel();
            this.labelNum = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Controls.Add(this.labelText, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ButtonUp, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.ButtonDown, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelNum, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(910, 139);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // labelText
            // 
            this.labelText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labelText.AutoSize = true;
            this.labelText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(197)))), ((int)(((byte)(62)))));
            this.labelText.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelText.ForeColor = System.Drawing.Color.White;
            this.labelText.Location = new System.Drawing.Point(3, 0);
            this.labelText.Name = "labelText";
            this.tableLayoutPanel1.SetRowSpan(this.labelText, 2);
            this.labelText.Size = new System.Drawing.Size(52, 139);
            this.labelText.TabIndex = 3;
            this.labelText.Text = "Text";
            this.labelText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ButtonUp
            // 
            this.ButtonUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(197)))), ((int)(((byte)(62)))));
            this.ButtonUp.Location = new System.Drawing.Point(873, 3);
            this.ButtonUp.Name = "ButtonUp";
            this.ButtonUp.Size = new System.Drawing.Size(34, 63);
            this.ButtonUp.TabIndex = 0;
            this.ButtonUp.Click += new System.EventHandler(this.ButtonUp_Click);
            this.ButtonUp.Paint += new System.Windows.Forms.PaintEventHandler(this.ButtonUp_Paint);
            this.ButtonUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonUp_MouseDown);
            this.ButtonUp.MouseLeave += new System.EventHandler(this.ButtonUp_MouseLeave);
            this.ButtonUp.MouseHover += new System.EventHandler(this.ButtonUp_MouseHover);
            this.ButtonUp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ButtonUp_MouseUp);
            // 
            // ButtonDown
            // 
            this.ButtonDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(197)))), ((int)(((byte)(62)))));
            this.ButtonDown.Location = new System.Drawing.Point(873, 72);
            this.ButtonDown.Name = "ButtonDown";
            this.ButtonDown.Size = new System.Drawing.Size(34, 64);
            this.ButtonDown.TabIndex = 1;
            this.ButtonDown.Click += new System.EventHandler(this.ButtonDown_Click);
            this.ButtonDown.Paint += new System.Windows.Forms.PaintEventHandler(this.ButtonDown_Paint);
            this.ButtonDown.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonDown_MouseDown);
            this.ButtonDown.MouseLeave += new System.EventHandler(this.ButtonDown_MouseLeave);
            this.ButtonDown.MouseHover += new System.EventHandler(this.ButtonDown_MouseHover);
            this.ButtonDown.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ButtonDown_MouseUp);
            // 
            // labelNum
            // 
            this.labelNum.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelNum.AutoSize = true;
            this.labelNum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(197)))), ((int)(((byte)(62)))));
            this.labelNum.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelNum.ForeColor = System.Drawing.Color.White;
            this.labelNum.Location = new System.Drawing.Point(813, 0);
            this.labelNum.Name = "labelNum";
            this.tableLayoutPanel1.SetRowSpan(this.labelNum, 2);
            this.labelNum.Size = new System.Drawing.Size(54, 139);
            this.labelNum.TabIndex = 2;
            this.labelNum.Text = "0";
            this.labelNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Numeric
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(197)))), ((int)(((byte)(62)))));
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Numeric";
            this.Size = new System.Drawing.Size(910, 139);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Numeric_Paint);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelText;
        private System.Windows.Forms.Panel ButtonUp;
        private System.Windows.Forms.Panel ButtonDown;
        private System.Windows.Forms.Label labelNum;
    }
}
