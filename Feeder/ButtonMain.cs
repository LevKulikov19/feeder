﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Feeder
{
    public partial class ButtonMain : UserControl
    {
        public delegate void ButtonMainEvent(object sender);
        public event ButtonMainEvent ClickButton;

        private Color buttonColor;
        public Color ButtonColor
        {
            get => buttonColor;
            set
            {
                buttonColor = value;
                BackColor = buttonColor;
                Refresh();
            }
        }
        public string ButtonText
        {
            get => labelText.Text;
            set
            {
                labelText.Text = value;
            }
        }

        public ButtonMain()
        {
            InitializeComponent();
            ButtonColor = BackColor;
        }

        private Color DarkenColor(Color colorIn, uint percent)
        {
            if (percent > 100) percent = 100;

            int a, r, g, b;
            a = colorIn.A;
            r = colorIn.R - (int)((colorIn.R / 100f) * percent);
            g = colorIn.G - (int)((colorIn.G / 100f) * percent);
            b = colorIn.B - (int)((colorIn.B / 100f) * percent);

            return Color.FromArgb(a, r, g, b);
        }

        private Color LightenColor(Color colorIn, uint percent)
        {
            if (percent > 100) percent = 100;

            int a, r, g, b;
            a = colorIn.A;
            r = colorIn.R + (int)(((255f - colorIn.R) / 100f) * percent);
            g = colorIn.G + (int)(((255f - colorIn.G) / 100f) * percent);
            b = colorIn.B + (int)(((255f - colorIn.B) / 100f) * percent);

            return Color.FromArgb(a, r, g, b);
        }

        private void ButtonMain_MouseHover(object sender, EventArgs e)
        {
            BackColor = LightenColor(ButtonColor, 10);
            Refresh();
        }

        private void ButtonMain_MouseLeave(object sender, EventArgs e)
        {
            BackColor = ButtonColor;
            Refresh();
        }

        private void ButtonMain_MouseDown(object sender, MouseEventArgs e)
        {
            BackColor = DarkenColor(ButtonColor, 10);
            Refresh();
        }

        private void ButtonMain_MouseUp(object sender, MouseEventArgs e)
        {
            BackColor = ButtonColor;
            Refresh();
        }

        private void labelText_Click(object sender, EventArgs e)
        {
            ClickButton(this);
        }
    }
}
